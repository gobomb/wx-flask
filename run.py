# -*- coding: utf-8 -*-
# filename: main.py
import web
from handle import Handle  # 将Handle类放进handle.py文件中，再导入进来
from testHandle import TestHandle
urls = (
    '/wx', 'Handle',
    '/testUpload', 'TestHandle',
)

if __name__ == '__main__':
    app = web.application(urls, globals())
    app.run()


