# -*- coding: utf-8 -*-
# filename: media.py
from basic import Basic
import urllib.request as ur
import poster3.encode
from poster3.streaminghttp import register_openers



# s = ur.urlopen("http://www.google.com")
# sl = s.read()


class Media(object):
    def __init__(self):
        register_openers()
    
    # 上传图片
    def upload(self, accessToken, filePath, mediaType):
        openFile = open(filePath, "rb")
        param = {'media': openFile}
        postData, postHeaders = poster3.encode.multipart_encode(param)

        postUrl = "https://api.weixin.qq.com/cgi-bin/media/upload?access_token=%s&type=%s" % (
            accessToken, mediaType)
        urlResp = ur.urlopen(postUrl, postData, postHeaders)
        got = urlResp.read()
        print (got)
        return got

if __name__ == '__main__':
    myMedia = Media()
    accessToken = Basic().get_access_token()
    filePath = "/Users/bombc/Documents/1.png"  # 请按实际填写
    mediaType = "image"
    myMedia.upload(accessToken, filePath, mediaType)
