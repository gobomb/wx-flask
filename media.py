# -*- coding: utf-8 -*-
# filename: media.py
import urllib.request as ur
import poster3.encode
from poster3.streaminghttp import register_openers
import requests

class Media(object):
    def __init__(self):
        register_openers()
    
    def get(self, accessToken, mediaId, filePath):   
        try: 
            postUrl = "https://api.weixin.qq.com/cgi-bin/media/get?access_token=%s&media_id=%s" % (
                accessToken, mediaId)
            print("getting",postUrl)
            urlResp=ur.urlopen(postUrl)
            print("getting2",urlResp)
            

            headers = urlResp.headers
            ct=headers["Content-Type"]
            print("getting3",headers["Content-Type"])

            if ct.startswith('application/json'):                
                print("fail to get image")
                # print(urlResp.json())
                return ct+"fail to get image"
            else:
                print("getting4",filePath)

                with open(filePath, "wb") as f:
                    f.write(urlResp.read()) # 将内容写入图片

                print("getting6")

                print("get successful")
                return filePath
        except Exception as Argument:
            print("got exception", Argument)
            return Argument


    # 上传图片
    def upload(self, accessToken, filePath, mediaType):
        try:
            files = {'file': open(filePath, 'rb')}
            postUrl = "https://api.weixin.qq.com/cgi-bin/media/upload?access_token=%s&type=%s" % (
                accessToken, mediaType)
            r = requests.post(postUrl, files=files)
            print(r)
            j=r.json()
            print(j)

            id=j['media_id']  #将字符串转为列表，方便更新列表（列表中每个元素都是一个单个字典）元素

            

            print("uploading2",id)

            return id

            openFile = open(filePath, "rb")
            openFile=openFile.read()
            # openFile = to_bytes(openFile)
            param = {'media': openFile}
            # print(openFile)

            postData, postHeaders = poster3.encode.multipart_encode(param)
            print("uploading2")

            postUrl = "https://api.weixin.qq.com/cgi-bin/media/upload?access_token=%s&type=%s" % (
                accessToken, mediaType)
            print(postUrl)


            urlResp = ur.urlopen(postUrl, data=postData)
            print("uploading3")
            got = urlResp.read()
            print (got)
            return got
        except Exception as Argument:
            print("got exception", Argument)
            return Argument

