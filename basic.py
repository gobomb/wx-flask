# -*- coding: utf-8 -*-
# filename: basic.py
import urllib.request as ur
import time
import json
import config
import os

appId = os.environ.get("appId", 'root')
appSecret = os.environ.get("appSecret", 'root')

class Basic:
    def __init__(self):
        self.__accessToken = ''
        self.__leftTime = 0

    def getIS(self):
        return appId+";"+appSecret

    def get_access_token(self):
        '''
        文档地址：https://developers.weixin.qq.com/doc/offiaccount/Basic_Information/Get_access_token.html
        正常返回：{"access_token":"ACCESS_TOKEN","expires_in":7200}
        异常返回：{"errcode":40013,"errmsg":"invalid appid"}
        :return:
        '''
        try:
            url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s" % (appId, appSecret)
            print(url)

            accessToken = json.loads(ur.urlopen(url).read().decode("utf-8"))
            print(accessToken)
            return None if "errcode" in accessToken else accessToken["access_token"]

        except Exception as Argument:
            print("got exception", Argument)
            return Argument
    # def __real_get_access_token(self):
    #     postUrl = ("https://api.weixin.qq.com/cgi-bin/token?grant_type="
    #                "client_credential&appid=%s&secret=%s" % (config.appId, config.appSecret))
    #     print("postUrl ",postUrl)

    #     urlResp = ur.urlopen(postUrl)
    #     urlResp = json.loads(urlResp.read())
    #     self.__accessToken = urlResp['access_token']
    #     self.__leftTime = urlResp['expires_in']

    #     print("time ", self.__leftTime)

    # def get_access_token(self):
    #     if self.__leftTime < 10:
    #         self.__real_get_access_token()
    #     return self.__accessToken

    # def run(self):
    #     while(True):
    #         if self.__leftTime > 10:
    #             time.sleep(2)
    #             self.__leftTime -= 2
    #         else:
    #             self.__real_get_access_token()
