import os

# 读取数据库环境变量
appId = os.environ.get("appId", 'root')
appSecret = os.environ.get("appSecret", 'root')
db_address = os.environ.get("MYSQL_ADDRESS", '127.0.0.1:3306')
